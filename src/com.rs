use crate::error::HrError;

use winapi::um::combaseapi::CoUninitialize;
use winapi::um::objbase::CoInitialize;

use std::ptr;

pub struct Com;

impl Com {
    pub fn new() -> Result<Self, HrError> {
        throw_if_failed!(
            unsafe { CoInitialize(ptr::null_mut()) },
            "Com Library Initialize Failed."
        );
        Ok(Self)
    }
}

impl Drop for Com {
    fn drop(&mut self) {
        unsafe { CoUninitialize() };
    }
}
