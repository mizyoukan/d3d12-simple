#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[macro_use]
mod error;
mod com;

use crate::com::Com;
use crate::error::HrError;

use winapi::shared::dxgi::IDXGIAdapter;
use winapi::shared::dxgi::IDXGISwapChain;
use winapi::shared::dxgi::DXGI_SWAP_CHAIN_DESC;
use winapi::shared::dxgi::DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
use winapi::shared::dxgi::DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
use winapi::shared::dxgi1_3::CreateDXGIFactory2;
use winapi::shared::dxgi1_4::IDXGIFactory4;
use winapi::shared::dxgiformat::DXGI_FORMAT;
use winapi::shared::dxgiformat::DXGI_FORMAT_R8G8B8A8_UNORM;
use winapi::shared::dxgitype::DXGI_USAGE_RENDER_TARGET_OUTPUT;
use winapi::shared::dxgitype::DXGI_USAGE_SHADER_INPUT;
use winapi::shared::winerror::FAILED;
use winapi::um::d3d12::D3D12CreateDevice;
use winapi::um::d3d12::ID3D12CommandAllocator;
use winapi::um::d3d12::ID3D12CommandList;
use winapi::um::d3d12::ID3D12CommandQueue;
use winapi::um::d3d12::ID3D12DescriptorHeap;
use winapi::um::d3d12::ID3D12Device;
use winapi::um::d3d12::ID3D12Fence;
use winapi::um::d3d12::ID3D12GraphicsCommandList;
use winapi::um::d3d12::ID3D12Resource;
use winapi::um::d3d12::D3D12_COMMAND_LIST_TYPE_DIRECT;
use winapi::um::d3d12::D3D12_COMMAND_QUEUE_DESC;
use winapi::um::d3d12::D3D12_COMMAND_QUEUE_FLAG_NONE;
use winapi::um::d3d12::D3D12_CPU_DESCRIPTOR_HANDLE;
use winapi::um::d3d12::D3D12_DESCRIPTOR_HEAP_DESC;
use winapi::um::d3d12::D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
use winapi::um::d3d12::D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
use winapi::um::d3d12::D3D12_FENCE_FLAG_NONE;
use winapi::um::d3d12::D3D12_RESOURCE_BARRIER;
use winapi::um::d3d12::D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
use winapi::um::d3d12::D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
use winapi::um::d3d12::D3D12_RESOURCE_STATES;
use winapi::um::d3d12::D3D12_RESOURCE_STATE_PRESENT;
use winapi::um::d3d12::D3D12_RESOURCE_STATE_RENDER_TARGET;
use winapi::um::d3d12::D3D12_RESOURCE_TRANSITION_BARRIER;
use winapi::um::d3d12::D3D12_VIEWPORT;
use winapi::um::d3dcommon::D3D_FEATURE_LEVEL_11_0;
use winapi::um::handleapi::CloseHandle;
use winapi::um::synchapi::CreateEventW;
use winapi::um::synchapi::WaitForSingleObject;
use winapi::um::winbase::INFINITE;
use winapi::um::winnt::HANDLE;
use winapi::Interface;

use winit::dpi::LogicalSize;
use winit::dpi::PhysicalSize;
use winit::os::windows::WindowExt as _;
use winit::ControlFlow;
use winit::EventsLoop;
use winit::Window;
use winit::WindowBuilder;
use winit::WindowEvent;

use std::env;
use std::mem::{self, MaybeUninit};
use std::ptr;

const BUFFER_COUNT: u32 = 2;
const SWAP_CHAIN_FORMAT: DXGI_FORMAT = DXGI_FORMAT_R8G8B8A8_UNORM;

struct App {
    device: *mut ID3D12Device,
    cmd_allocator: *mut ID3D12CommandAllocator,
    cmd_queue: *mut ID3D12CommandQueue,
    cmd_list: *mut ID3D12GraphicsCommandList,
    adapter: *mut IDXGIAdapter,
    factory: *mut IDXGIFactory4,
    swap_chain: *mut IDXGISwapChain,
    descriptor_heap: *mut ID3D12DescriptorHeap,
    color_target: *mut ID3D12Resource,
    color_target_handle: D3D12_CPU_DESCRIPTOR_HANDLE,
    fence: *mut ID3D12Fence,
    fence_event: HANDLE,
    viewport: D3D12_VIEWPORT,
}

impl App {
    fn new(window: &Window) -> Result<Self, HrError> {
        // ウィンドウ幅を取得
        let PhysicalSize { width, height } = window
            .get_inner_size()
            .unwrap()
            .to_physical(window.get_hidpi_factor());
        log::info!("width: {} height: {}", width, height);

        let mut factory = MaybeUninit::<*mut IDXGIFactory4>::uninit();
        throw_if_failed!(
            unsafe { CreateDXGIFactory2(0, &IDXGIFactory4::uuidof(), factory.as_mut_ptr().cast()) },
            "CreateDXGIFactory() Failed."
        );
        let factory = unsafe { factory.assume_init() };

        let mut adapter = MaybeUninit::<*mut IDXGIAdapter>::uninit();
        throw_if_failed!(
            unsafe { (*factory).EnumAdapters(0, adapter.as_mut_ptr()) },
            "IDXGIFactory::EnumAdapters() Failed."
        );
        let adapter = unsafe { adapter.assume_init() };

        // デバイス生成
        let mut device = MaybeUninit::<*mut ID3D12Device>::uninit();
        let hr = unsafe {
            D3D12CreateDevice(
                adapter.cast(),
                D3D_FEATURE_LEVEL_11_0,
                &ID3D12Device::uuidof(),
                device.as_mut_ptr().cast(),
            )
        };
        // 生成チェック
        if FAILED(hr) {
            // Warpアダプターで再トライ
            unsafe { (*adapter).Release() };

            let mut adapter = MaybeUninit::<*mut IDXGIAdapter>::uninit();
            throw_if_failed!(
                unsafe {
                    (*factory).EnumWarpAdapter(&IDXGIAdapter::uuidof(), adapter.as_mut_ptr().cast())
                },
                "IDXGIFactory::EnumWarpAdapter() Failed."
            );
            let adapter = unsafe { adapter.assume_init() };

            // デバイス生成
            let mut device = MaybeUninit::<*mut ID3D12Device>::uninit();
            throw_if_failed!(
                unsafe {
                    D3D12CreateDevice(
                        adapter.cast(),
                        D3D_FEATURE_LEVEL_11_0,
                        &ID3D12Device::uuidof(),
                        device.as_mut_ptr().cast(),
                    )
                },
                "D3D12CreateDevice() Failed."
            );
        }
        let device = unsafe { device.assume_init() };

        // コマンドアロケータを生成
        let mut cmd_allocator = MaybeUninit::<*mut ID3D12CommandAllocator>::uninit();
        throw_if_failed!(
            unsafe {
                (*device).CreateCommandAllocator(
                    D3D12_COMMAND_LIST_TYPE_DIRECT,
                    &ID3D12CommandAllocator::uuidof(),
                    cmd_allocator.as_mut_ptr().cast(),
                )
            },
            "ID3D12Device::CreateCommandAllocator() Failed."
        );
        let cmd_allocator = unsafe { cmd_allocator.assume_init() };

        // コマンドキューを生成
        let mut desc = D3D12_COMMAND_QUEUE_DESC::default();
        desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
        desc.Priority = 0;
        desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        let mut cmd_queue = MaybeUninit::<*mut ID3D12CommandQueue>::uninit();
        throw_if_failed!(
            unsafe {
                (*device).CreateCommandQueue(
                    &desc,
                    &ID3D12CommandQueue::uuidof(),
                    cmd_queue.as_mut_ptr().cast(),
                )
            },
            "ID3D12Device::CreateCommandQueue() Failed."
        );
        let cmd_queue = unsafe { cmd_queue.assume_init() };

        // スワップチェインを生成
        let mut desc = DXGI_SWAP_CHAIN_DESC::default();
        desc.BufferCount = BUFFER_COUNT;
        desc.BufferDesc.Format = SWAP_CHAIN_FORMAT;
        desc.BufferDesc.Width = width as u32;
        desc.BufferDesc.Height = height as u32;
        desc.BufferDesc.RefreshRate.Numerator = 60;
        desc.BufferDesc.RefreshRate.Denominator = 1;
        desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
        desc.OutputWindow = window.get_hwnd().cast();
        desc.SampleDesc.Count = 1;
        desc.SampleDesc.Quality = 0;
        desc.Windowed = 1;
        desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
        desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

        // アダプター単位の処理にマッチするのはdeviceではなくcmd_queueなので、cmd_queueを第一引数として渡す
        let mut swap_chain = MaybeUninit::<*mut IDXGISwapChain>::uninit();
        throw_if_failed!(
            unsafe {
                (*factory).CreateSwapChain(cmd_queue.cast(), &mut desc, swap_chain.as_mut_ptr())
            },
            "IDXGIFactory::CreateSwapChain() Failed."
        );
        let swap_chain = unsafe { swap_chain.assume_init() };

        // デスクリプタヒープの生成
        let mut desc = D3D12_DESCRIPTOR_HEAP_DESC::default();
        desc.NumDescriptors = 1;
        desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        let mut descriptor_heap = MaybeUninit::<*mut ID3D12DescriptorHeap>::uninit();
        throw_if_failed!(
            unsafe {
                (*device).CreateDescriptorHeap(
                    &desc,
                    &ID3D12DescriptorHeap::uuidof(),
                    descriptor_heap.as_mut_ptr().cast(),
                )
            },
            "ID3D12Device::CreateDescriptorHeap() Failed."
        );
        let descriptor_heap = unsafe { descriptor_heap.assume_init() };

        // コマンドリストの生成
        let mut cmd_list = MaybeUninit::<*mut ID3D12GraphicsCommandList>::uninit();
        throw_if_failed!(
            unsafe {
                (*device).CreateCommandList(
                    1,
                    D3D12_COMMAND_LIST_TYPE_DIRECT,
                    cmd_allocator,
                    ptr::null_mut(),
                    &ID3D12GraphicsCommandList::uuidof(),
                    cmd_list.as_mut_ptr().cast(),
                )
            },
            "ID3D12Device::CreateCommandList() Failed."
        );
        let cmd_list = unsafe { cmd_list.assume_init() };

        // バックバッファからレンダーターゲットを生成
        let mut color_target = MaybeUninit::<*mut ID3D12Resource>::uninit();
        throw_if_failed!(
            unsafe {
                (*swap_chain).GetBuffer(
                    0,
                    &ID3D12Resource::uuidof(),
                    color_target.as_mut_ptr().cast(),
                )
            },
            "IDXGISwapChain::GetBuffer() Failed."
        );
        let color_target = unsafe { color_target.assume_init() };
        let color_target_handle =
            unsafe { (*descriptor_heap).GetCPUDescriptorHandleForHeapStart() };
        unsafe {
            (*device).CreateRenderTargetView(color_target, ptr::null_mut(), color_target_handle)
        };

        // フェンスの生成
        let fence_event = unsafe { CreateEventW(ptr::null_mut(), 0, 0, ptr::null_mut()) };
        let mut fence = MaybeUninit::<*mut ID3D12Fence>::uninit();
        throw_if_failed!(
            unsafe {
                (*device).CreateFence(
                    0,
                    D3D12_FENCE_FLAG_NONE,
                    &ID3D12Fence::uuidof(),
                    fence.as_mut_ptr().cast(),
                )
            },
            "ID3D12Device::CreateFence() Failed."
        );
        let fence = unsafe { fence.assume_init() };

        // ビューポートの設定.
        let mut viewport = D3D12_VIEWPORT::default();
        viewport.TopLeftX = 0.0;
        viewport.TopLeftY = 0.0;
        viewport.Width = width as f32;
        viewport.Height = height as f32;
        viewport.MinDepth = 0.0;
        viewport.MaxDepth = 1.0;

        Ok(Self {
            device,
            cmd_allocator,
            cmd_queue,
            cmd_list,
            adapter,
            factory,
            swap_chain,
            descriptor_heap,
            color_target,
            color_target_handle,
            fence,
            fence_event,
            viewport,
        })
    }

    pub fn on_resize(&mut self, size: &PhysicalSize) -> Result<(), HrError> {
        self.viewport.Width = size.width as f32;
        self.viewport.Height = size.height as f32;

        // レンダーターゲットを破棄
        unsafe { (*self.color_target).Release() };

        // バックバッファをリサイズ
        throw_if_failed!(
            unsafe { (*self.swap_chain).ResizeBuffers(BUFFER_COUNT, 0, 0, SWAP_CHAIN_FORMAT, 0) },
            "IDXGISwapChain::ResizeBuffers() Failed."
        );

        // バックバッファを取得
        let mut new_color_target = MaybeUninit::<*mut ID3D12Resource>::uninit();
        throw_if_failed!(
            unsafe {
                (*self.swap_chain).GetBuffer(
                    0,
                    &ID3D12Resource::uuidof(),
                    new_color_target.as_mut_ptr().cast(),
                )
            },
            "IDXGISwapChain::GetBuffer() Failed."
        );
        self.color_target = unsafe { new_color_target.assume_init() };

        // レンダーターゲットを生成
        let color_target_handle =
            unsafe { (*self.descriptor_heap).GetCPUDescriptorHandleForHeapStart() };
        unsafe {
            (*self.device).CreateRenderTargetView(
                self.color_target,
                ptr::null_mut(),
                color_target_handle,
            )
        };

        Ok(())
    }

    pub fn on_render(&self) -> Result<(), HrError> {
        // ビューポートを設定
        unsafe { (*self.cmd_list).RSSetViewports(1, &self.viewport) };
        set_resource_barrier(
            self.cmd_list,
            self.color_target,
            D3D12_RESOURCE_STATE_PRESENT,
            D3D12_RESOURCE_STATE_RENDER_TARGET,
        );

        // カラーバッファをクリア
        let clear_color: [f32; 4] = [0.39, 0.58, 0.92, 1.0];
        unsafe {
            (*self.cmd_list).ClearRenderTargetView(
                self.color_target_handle,
                &clear_color,
                0,
                ptr::null_mut(),
            )
        };
        set_resource_barrier(
            self.cmd_list,
            self.color_target,
            D3D12_RESOURCE_STATE_RENDER_TARGET,
            D3D12_RESOURCE_STATE_PRESENT,
        );

        self.present(0)
    }

    fn present(&self, sync_interval: u32) -> Result<(), HrError> {
        // コマンドリストへの記録を終了し、コマンド実行
        throw_if_failed!(
            unsafe { (*self.cmd_list).Close() },
            "ID3D12CommandList::Close() Failed."
        );
        let cmd_lists: Vec<*mut ID3D12CommandList> = vec![self.cmd_list.cast()];
        unsafe {
            (*self.cmd_queue).ExecuteCommandLists(cmd_lists.len() as u32, cmd_lists.as_ptr())
        };

        // コマンドの実行の終了を待機する
        throw_if_failed!(
            unsafe { (*self.fence).Signal(0) },
            "ID3D12Fence::Signal() Failed."
        );
        throw_if_failed!(
            unsafe { (*self.fence).SetEventOnCompletion(1, self.fence_event) },
            "ID3D12Fence::SetEventOnCompletion() Failed."
        );
        throw_if_failed!(
            unsafe { (*self.cmd_queue).Signal(self.fence, 1) },
            "ID3D12CommandQueue::Signal() Failed."
        );
        unsafe { WaitForSingleObject(self.fence_event, INFINITE) };

        // 画面に表示する
        throw_if_failed!(
            unsafe { (*self.swap_chain).Present(sync_interval, 0) },
            "IDXGISwapChain::Present() Failed."
        );

        // コマンドリストとコマンドアロケータをリセットする
        throw_if_failed!(
            unsafe { (*self.cmd_allocator).Reset() },
            "ID3D12CommandAllocator::Reset() Failed."
        );
        throw_if_failed!(
            unsafe { (*self.cmd_list).Reset(self.cmd_allocator, ptr::null_mut()) },
            "ID3D12CommandList::Reset() Failed."
        );

        Ok(())
    }
}

impl Drop for App {
    fn drop(&mut self) {
        unsafe {
            (*self.fence).Release();
            CloseHandle(self.fence_event);
            (*self.color_target).Release();
            (*self.cmd_list).Release();
            (*self.descriptor_heap).Release();
            (*self.swap_chain).Release();
            (*self.cmd_queue).Release();
            (*self.cmd_allocator).Release();
            (*self.device).Release();
            (*self.adapter).Release();
            (*self.factory).Release();
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if cfg!(debug_assertions) {
        env::set_var("RUST_LOG", "debug");
        env_logger::init();
    }

    // COMライブラリの初期化
    let _com = Com::new()?;

    // ウィンドウを生成
    let mut events_loop = EventsLoop::new();
    let window = WindowBuilder::new()
        .with_title("SimpleSample")
        .with_dimensions(LogicalSize::new(960.0, 540.0))
        .with_visibility(false)
        .build(&mut events_loop)?;

    let mut app = App::new(&window)?;

    window.show();

    events_loop.run_forever(move |event| match event {
        winit::Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            mem::drop(&mut app);
            ControlFlow::Break
        }
        winit::Event::WindowEvent {
            event: WindowEvent::Resized(logical_size),
            ..
        } => {
            let physical_size = logical_size.to_physical(window.get_hidpi_factor());
            app.on_resize(&physical_size)
                .expect("App::on_resize() Failed");
            ControlFlow::Continue
        }
        _ => {
            app.on_render().expect("App::on_render() Failed");
            ControlFlow::Continue
        }
    });

    Ok(())
}

fn set_resource_barrier(
    cmd_list: *mut ID3D12GraphicsCommandList,
    resource: *mut ID3D12Resource,
    state_before: D3D12_RESOURCE_STATES,
    state_after: D3D12_RESOURCE_STATES,
) {
    let mut desc = D3D12_RESOURCE_BARRIER::default();
    desc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    unsafe {
        *desc.u.Transition_mut() = D3D12_RESOURCE_TRANSITION_BARRIER {
            pResource: resource,
            Subresource: D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
            StateBefore: state_before,
            StateAfter: state_after,
        };
    }

    unsafe { (*cmd_list).ResourceBarrier(1, &desc) };
}
