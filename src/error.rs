use winapi::um::winnt::HRESULT;

use std::error::Error;
use std::fmt;

pub struct HrError(pub HRESULT);

impl fmt::Debug for HrError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "HrError(HRESULT: 0x{:08x})", self.0)
    }
}

impl fmt::Display for HrError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "HrError(HRESULT: 0x{:08x})", self.0)
    }
}

impl Error for HrError {}

#[macro_export]
macro_rules! throw_if_failed {
    ($expr:expr, $msg:expr) => {
        let hr = $expr;
        // FAILED(hr)
        if hr < 0 {
            log::error!("{} HRESULT: 0x{:08x}", $msg, hr);
            return Err(HrError(hr))?;
        }
    };
}
